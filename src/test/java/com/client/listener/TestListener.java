package com.client.listener;

import javax.jms.Message;
import javax.jms.MessageListener;

import org.apache.log4j.Logger;

/**
 * 
 * Setup a listener to receive messages.
 *
 */
public class TestListener implements MessageListener {

	Logger logger = Logger.getLogger(TestListener.class);
	
	public void onMessage(Message message) {
	
		logger.info("Received Message " + message);
		
	}
	 
}
