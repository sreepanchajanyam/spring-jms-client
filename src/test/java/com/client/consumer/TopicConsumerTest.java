package com.client.consumer;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.client.listener.TestListener;



public class TopicConsumerTest {
	
	private String brokerURL1 = "tcp://localhost:61616";
	TestListener testListener = new TestListener();
	
	@Test
	public void testDurableTopic() throws InterruptedException {
		
		Consumer consumer = ConsumerBuilder.getBuilder(brokerURL1, "test.topic",testListener)
		.withCachedSessionSize(500)
		.withConcurrentConsumers(1)
		.withPubSubDomain(true) // make the destination a topic
		.register();
		
		assertNotNull(consumer);
		Thread.sleep(600000); // sleep for 10 minutes - only for testing. You don't have to do this in actual environment.
	
	}
	
	
}
