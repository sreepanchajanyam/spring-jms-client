package com.client.consumer;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.client.listener.TestListener;



public class QueueConsumerTest {
	
	private String brokerURL1 = "tcp://localhost:61616";
	TestListener testListener = new TestListener();
	
	@Test
	public void testQueue() throws InterruptedException {
		Consumer consumer = ConsumerBuilder.getBuilder(brokerURL1, "test.queue",testListener)
		.withCachedSessionSize(500)
		.withConcurrentConsumers(10) // concurrent consumers to improve throughput
		.register();
		assertNotNull(consumer);
		Thread.sleep(600000); // sleep for 10 minutes - only for testing. You don't have to do this in actual environment.
		
	}	
	
}
