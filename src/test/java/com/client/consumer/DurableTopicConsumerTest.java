package com.client.consumer;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.client.listener.TestListener;



public class DurableTopicConsumerTest {
	
	private String brokerURL1 = "tcp://localhost:61616";
	TestListener testListener = new TestListener();
	
	@Test
	public void testDurableTopic() throws InterruptedException {
		TestListener testListener = new TestListener();
		
		Consumer consumer = ConsumerBuilder.getBuilder(brokerURL1, "test.durable.topic",testListener)
		.withCachedSessionSize(500)
		.withConcurrentConsumers(1)
		.withPubSubDomain(true) // to make the destination a topic
		.asDurableSubscriber(true) // to make the topic durable 
		.withClientId("1234") // set a client id for the durable topic
		.withDurableSubscriptionName("sample_pos_1234") // set a subscriptionName for the durable topic
		.register();
		
		assertNotNull(consumer);
		Thread.sleep(600000); // sleep for 10 minutes - only for testing. You don't have to do this in actual environment.
	
	}
	
	
}
