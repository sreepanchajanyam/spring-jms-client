package com.client.integrated;

import junit.framework.TestCase;

import org.junit.Test;

import com.client.consumer.Consumer;
import com.client.consumer.ConsumerBuilder;
import com.client.creator.TestMessageCreator;
import com.client.listener.TestListener;
import com.client.model.Order;
import com.client.producer.Producer;
import com.client.producer.ProducerBuilder;

public class VirtualTopicTest extends TestCase {
	
	private TestListener testListener = new TestListener();;
	
	private String brokerURL1 = "tcp://localhost:61676";

		
	@Test
	public void testVirtualTopic() throws InterruptedException {
		
		Consumer virtualConsumerA 
				= ConsumerBuilder
				.getBuilder(brokerURL1, "Consumer.OrderDataWarehouse.VirtualTopic.smart.order",testListener)
				.withCachedSessionSize(500)
				.withConcurrentConsumers(10)
				.register();
		
		Consumer virtualConsumerB 
				= ConsumerBuilder
				.getBuilder(brokerURL1, "Consumer.OrderFulfillment.VirtualTopic.smart.order",testListener)
				.withMessageSelector("companyid='pos'")
				.withCachedSessionSize(500)
				.withConcurrentConsumers(10)
				.register();

		Producer producer 
				= ProducerBuilder
				.getBuilder(brokerURL1, "VirtualTopic.smart.order")
				.withPubSubDomain(true)
				.build();
		
		producer.sendMessage(new TestMessageCreator(new Order(123,"123")));
		
		
		assertNotNull(virtualConsumerA);
		assertNotNull(virtualConsumerB);
		assertNotNull(producer);
		Thread.sleep(6000000);
	}
	
	}

