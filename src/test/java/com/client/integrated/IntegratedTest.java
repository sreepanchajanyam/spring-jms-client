package com.client.integrated;

import org.junit.Test;

import com.client.consumer.ConsumerBuilder;
import com.client.listener.TestListener;
import com.client.producer.ProducerBuilder;

public class IntegratedTest {
	
	
	private String brokerURL1 = "tcp://localhost:61616";
	private TestListener testListener = new TestListener();
	
	
	/**
	 * Register a listener as a consumer on a queue
	 * Produce messages to the queue
	 * Receive messages from the queue
	 */
	@Test
	public void testQueue() {
		ConsumerBuilder.getBuilder(brokerURL1, "test.queue",testListener)
		.withCachedSessionSize(500)
		.withConcurrentConsumers(10)
		.register();
		
		ProducerBuilder.getBuilder(brokerURL1, "test.queue")
		.build()
		.sendMessage("First Message");
		
	}
	
	/**
	 * Register a listener as a non-durable subscription on a topic
	 * Produce messages to the topic
	 * Receive messages from the topic
	 */
	
	@Test
	public void testTopic() {	
		ConsumerBuilder.getBuilder(brokerURL1, "test.topic",testListener)
		.withCachedSessionSize(500)
		.withConcurrentConsumers(1)
		.withPubSubDomain(true)
		.register();

		ProducerBuilder.getBuilder(brokerURL1, "test.topic")
		.withPubSubDomain(true) // produce to a topic
		.build()
		.sendMessage("First Message");
	}
	
	
	/**
	 * Register a listener as a durable subscription on a topic
	 * Produce messages to the topic
	 * Receive messages from the topic
	 */
	
	@Test
	public void testDurableTopic() {
		ConsumerBuilder.getBuilder(brokerURL1, "test.durable.topic.1",testListener)
		.withCachedSessionSize(500)
		.withPubSubDomain(true)
		.withClientId("sample_123")
		.withDurableSubscriptionName("sample_pos_1234")
		.register();
		
		ProducerBuilder.getBuilder(brokerURL1, "test.durable.topic")
		.withPubSubDomain(true)// produce to a topic
		.build()
		.sendMessage("First Message");
	}
}
