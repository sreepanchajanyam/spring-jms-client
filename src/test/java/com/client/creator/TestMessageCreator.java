package com.client.creator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.MessageCreator;

import com.client.model.Order;

public class TestMessageCreator implements MessageCreator{

	private Order order;
	
	public TestMessageCreator(Order message) {
		this.order = message;
	}


	public Message createMessage(Session session) throws JMSException {
		Message message = session.createObjectMessage(order);
		message.setStringProperty("companyid", "pos");
		return message;
	}

}
