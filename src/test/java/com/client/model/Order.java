package com.client.model;

import java.io.Serializable;

public class Order implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int order_id;
	private String order_name;
	
	public Order(int order_id, String order_name) {
		this.order_id = order_id;
		this.order_name = order_name;
	}
	
}
