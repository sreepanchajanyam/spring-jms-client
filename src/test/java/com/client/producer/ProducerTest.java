package com.client.producer;

import org.junit.Test;

import com.client.producer.Producer;
import com.client.producer.ProducerBuilder;

import junit.framework.TestCase;

public class ProducerTest extends TestCase {
	
	private String brokerURL1 = "tcp://localhost:61616";
	
	/**
	 * Test a 2MB message.
	 */
	@Test
	public void testSendMessage(){
		StringBuilder stringB = new StringBuilder(2000000);
		ProducerBuilder.getBuilder(brokerURL1, "test.queue") 
		.build()
		.sendMessage(stringB);
	}
	
	@Test
	public void testSendMessageToQueue(){
		ProducerBuilder.getBuilder(brokerURL1, "test.queue") 
		.build()
		.sendMessage("Queue Message");
	}
	
	@Test
	public void testSendMessageToTopic(){
		ProducerBuilder.getBuilder(brokerURL1, "test.topic") 
		.withPubSubDomain(true) // false means queue, true means topic
		.build()
		.sendMessage("Topic Message");
	}
	
	@Test
	public void testSendMessageToDurableTopic(){
		ProducerBuilder.getBuilder(brokerURL1, "test.durable.topic")
		.withPubSubDomain(true) // false means queue, true means topic
		.build()
		.sendMessage("Durable Topic Message");
	}

}
