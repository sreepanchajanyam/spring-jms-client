package com.client.producer;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
/**
 * 
 * Register exactly one producer per JVM per destination through ProducerBuilder 
 * and reuse it.
 * Producer class to send message to a broker on a given destination
 * setter methods on this class are protected
 *
 */
public class Producer {

	private ActiveMQConnectionFactory amqConnectionFactory;
	private CachingConnectionFactory cachingConnectionFactory;
	private String brokerURL;
	private String destinationName;
	private int cachedSessionSize = 500;
	private boolean watchTopicAdvisories = false;
	private boolean deliveryPersistent = true;
	private long timeToLive = -1;
	private JmsTemplate jmsTemplate;
	private boolean pubSubDomain = false;
	private static final Logger logger = Logger.getLogger(Producer.class);
	
	
	/**
	 * Intentionally protected to restrict access to with in package and subclasses
	 * This should be accessible only within package
	 * @param brokerURL
	 * @param destinationName
	 * @param pubsubdomain
	 */
	protected Producer(String brokerURL, String destinationName){
		this.brokerURL =  brokerURL;
		this.destinationName = destinationName;
	}

	private void initConnection() {
		this.amqConnectionFactory = new ActiveMQConnectionFactory();
		this.amqConnectionFactory.setBrokerURL(brokerURL);
		this.amqConnectionFactory.setWatchTopicAdvisories(watchTopicAdvisories);
		this.cachingConnectionFactory = new CachingConnectionFactory(
				this.amqConnectionFactory);
		this.cachingConnectionFactory.setSessionCacheSize(cachedSessionSize);
	}
	
	public void init() {
		logger.info("Initializing Producer Connection to brokerURL : " + brokerURL + " on destination : " + destinationName);
		initConnection();
		
		jmsTemplate = new JmsTemplate();
		jmsTemplate.setConnectionFactory(cachingConnectionFactory);
		jmsTemplate.setExplicitQosEnabled(true);
		jmsTemplate.setDefaultDestinationName(destinationName);
		jmsTemplate.setPubSubDomain(pubSubDomain);
		jmsTemplate.setDeliveryPersistent(deliveryPersistent);
		jmsTemplate.setTimeToLive(timeToLive);
		logger.info("Initialized Producer with following properties : "  + this);
	}

	public void sendMessage(MessageCreator messageCreator) {
		jmsTemplate.send(messageCreator);
	}

	public void sendMessage(Object message) {
		jmsTemplate.convertAndSend(message);
	}
	public int getCachedSessionSize() {
		return cachedSessionSize;
	}

	protected void setCachedSessionSize(int cachedSessionSize) {
		this.cachedSessionSize = cachedSessionSize;
	}

	public boolean isWatchTopicAdvisories() {
		return watchTopicAdvisories;
	}

	protected void setWatchTopicAdvisories(boolean watchTopicAdvisories) {
		this.watchTopicAdvisories = watchTopicAdvisories;
	}

	public boolean isPubsubdomain() {
		return pubSubDomain;
	}

	public boolean isDeliveryPersistent() {
		return deliveryPersistent;
	}

	protected void setDeliveryPersistent(boolean deliveryPersistent) {
		this.deliveryPersistent = deliveryPersistent;
	}

	public long getTimeToLive() {
		return timeToLive;
	}

	protected void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}
	
	@Override
	public String toString() {
		return "Producer [amqConnectionFactory=" + amqConnectionFactory
				+ ", cachingConnectionFactory=" + cachingConnectionFactory
				+ ", brokerURL=" + brokerURL + ", destinationName="
				+ destinationName + ", cachedSessionSize=" + cachedSessionSize
				+ ", watchTopicAdvisories=" + watchTopicAdvisories
				+ ", pubsubdomain=" + pubSubDomain + ", deliveryPersistent="
				+ deliveryPersistent + ", timeToLive=" + timeToLive
				+ ", jmsTemplate=" + jmsTemplate + "]";
	}

	public boolean isPubSubDomain() {
		return pubSubDomain;
	}

	 protected void setPubSubDomain(boolean pubSubDomain) {
		this.pubSubDomain = pubSubDomain;
	}

	
	
	
}
