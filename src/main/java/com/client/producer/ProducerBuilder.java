package com.client.producer;

import org.apache.log4j.Logger;

public class ProducerBuilder {
	
	private Producer producer = null;
	private static final Logger logger = Logger.getLogger(ProducerBuilder.class);
	
	public static ProducerBuilder getBuilder(String brokerURL, String destinationName) {
		
		ProducerBuilder builder = new ProducerBuilder();
		builder.producer= new Producer(brokerURL, destinationName);
		return builder;
	}
	
	/**
	 * parameter to make a destination a topic.
	 * @param pubSubDomain
	 * @return
	 */
	public ProducerBuilder withPubSubDomain(boolean pubSubDomain) {
		producer.setPubSubDomain(pubSubDomain);
		return this;
	}
	
	/**
	 * cachedSessionSize - to cache JMS session.
	 * set it to a number like 500 to improve performance
	 * @param cachedSessionSize
	 * @return
	 */
	public ProducerBuilder withCachedSessionSize(int cachedSessionSize) {
		producer.setCachedSessionSize(cachedSessionSize);
		return this;
	}

	/**
	 * should not need to set this unless you have a good reason to
	 * default is false
	 * @param watchTopicAdvisories
	 * @return
	 */
	public ProducerBuilder withWatchTopicAdvisories(boolean watchTopicAdvisories) {
		producer.setWatchTopicAdvisories(watchTopicAdvisories);
		return this;
	}

	/**
	 * default message persistence = true for guaranteed delivery.
	 * change to false to make messages non-persistent
	 * @param deliveryPersistent
	 * @return
	 */
	public ProducerBuilder withDeliveryPersistent(boolean deliveryPersistent) {
		producer.setDeliveryPersistent(deliveryPersistent);
		return this;
	}


	/**
	 * expires the message after given number of milliseonds.
	 * default value is -1. This is for infinite persistence.
	 * @param timeToLive
	 * @return
	 */
	public ProducerBuilder withTimeToLive(long timeToLive) {
		producer.setTimeToLive(timeToLive);
		return this;
	}
	
	public Producer build() {
		producer.init();
		return producer;
	}

}
