package com.client.consumer;

import javax.jms.MessageListener;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.springframework.jca.cci.connection.SingleConnectionFactory;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import com.client.producer.Producer;

/**
 * 
 * Register exactly one consumer per JVM per destination through ConsumerBuilder
 * Once Consumer is registered Messages would be given to the registered
 * listener.
 *
 */
public class Consumer {

	private int concurrentConsumers = 1;
	private ActiveMQConnectionFactory amqConnectionFactory;
	private CachingConnectionFactory cachingConnectionFactory;
	private DefaultMessageListenerContainer listenerContainer;
	private String destinationName;
	private String brokerURL;
	private boolean containerStarted = false;
	private int cachedSessionSize = 500;
	private boolean watchTopicAdvisories = false;
	private boolean pubSubDomain;
	private int sessionAcknowledgeMode = Session.AUTO_ACKNOWLEDGE;
	private MessageListener messageListener;
	private String clientId;
	private boolean isDurable = false;
	private String subscriptionName;
	private String messageSelector;

	private static final Logger logger = Logger.getLogger(Consumer.class);

	public Consumer(String brokerURL, String destinationName,
			MessageListener eventListener) {
		this.brokerURL = brokerURL;
		this.destinationName = destinationName;
		this.messageListener = eventListener;

	}

	protected void init() {
		initConnection();
		initListenerContainer();
		start();
	}

	/**
	 * Initialize AMQConnectionFactory and set it to spring
	 * CachingConnectionFactory
	 */
	protected void initConnection() {
		this.amqConnectionFactory = new ActiveMQConnectionFactory();
		this.amqConnectionFactory.setBrokerURL(brokerURL);
		this.amqConnectionFactory.setWatchTopicAdvisories(watchTopicAdvisories);
		this.cachingConnectionFactory = new CachingConnectionFactory(
				this.amqConnectionFactory);
		this.cachingConnectionFactory.setSessionCacheSize(cachedSessionSize);
	}

	/**
	 * Initialize DefaultMessageListenerContainer
	 */

	protected void initListenerContainer() {

		this.listenerContainer = new DefaultMessageListenerContainer();
		this.listenerContainer
				.setConnectionFactory(this.cachingConnectionFactory);
		this.listenerContainer.setDestinationName(destinationName);
		this.listenerContainer.setBeanName(destinationName);
		this.listenerContainer.setPubSubDomain(pubSubDomain);
		this.listenerContainer
				.setSessionAcknowledgeMode(sessionAcknowledgeMode);
		this.listenerContainer.setConcurrentConsumers(concurrentConsumers);
		this.listenerContainer.setMessageListener(messageListener);
		this.listenerContainer.setSubscriptionDurable(isDurable);
		if (messageSelector != null && messageSelector.trim() != "") {
			this.listenerContainer.setMessageSelector(messageSelector);
		}
		if (clientId != null) {
			this.listenerContainer.setClientId(clientId);
			this.cachingConnectionFactory.setClientId(clientId);

		}
		if (subscriptionName != null) {
			this.listenerContainer.setDurableSubscriptionName(subscriptionName);
		}
	}

	/**
	 * Returns true if this call to the container has stopped the consumer else
	 * returns false. use isStarted() to check the status of the consumer
	 * 
	 * @return
	 */
	public boolean stop() {
		if (isStarted()) {
			this.listenerContainer.stop();
			this.containerStarted = false;
			logger.info("Stopped listenerContainer ( DMLC ) for destination: "
					+ destinationName + " brokerURL: " + brokerURL);
			return true;
		} else {
			logger.warn("Trying to stop a stopped listener ...");
			return false;
		}

	}

	/**
	 * Returns true if this call to the container has started the consumer else
	 * returns false. use isStarted() to check the status of the consumer
	 * 
	 * @return
	 */

	public boolean isStarted() {
		return this.containerStarted;
	}

	public boolean start() {
		if (!isStarted()) {
			this.listenerContainer.initialize();
			this.listenerContainer.start();
			this.containerStarted = true;
			logger.info("Container is started");
			return true;
		} else {
			logger.warn("Trying to start an up and running Listener Container");
			return false;
		}

	}

	public int getConcurrentConsumers() {
		return concurrentConsumers;
	}

	protected void setConcurrentConsumers(int concurrentConsumers) {
		this.concurrentConsumers = concurrentConsumers;
	}

	public boolean isWatchTopicAdvisories() {
		return watchTopicAdvisories;
	}

	protected void setWatchTopicAdvisories(boolean watchTopicAdvisories) {
		this.watchTopicAdvisories = watchTopicAdvisories;
	}

	public boolean isPubSubDomain() {
		return pubSubDomain;
	}

	protected void setPubSubDomain(boolean pubSubDomain) {
		this.pubSubDomain = pubSubDomain;
	}

	public int getSessionAcknowledgeMode() {
		return sessionAcknowledgeMode;
	}

	protected void setSessionAcknowledgeMode(int sessionAcknowledgeMode) {
		this.sessionAcknowledgeMode = sessionAcknowledgeMode;
	}

	public String getClientId() {
		return clientId;
	}

	protected void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getSubscriptionName() {
		return subscriptionName;
	}

	protected void setDurableSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}

	public int getCachedSessionSize() {
		return this.cachedSessionSize;
	}

	protected void setCachedSessionSize(int cachedSessionSize) {
		this.cachedSessionSize = cachedSessionSize;
	}

	protected void setDurablesubscription(boolean isDurable) {
		this.isDurable = isDurable;
	}

	public String getMessageSelector() {
		return messageSelector;
	}

	protected void setMessageSelector(String messageSelector) {
		this.messageSelector = messageSelector;
	}

}
