package com.client.consumer;

import javax.jms.MessageListener;


/**
 * 
 * Builder Class to build Consumer.
 *
 */
public class ConsumerBuilder {

	private Consumer consumer = null;

	public static ConsumerBuilder getBuilder(String brokerURL,
			String destinationName, MessageListener messageListener) {

		ConsumerBuilder builder = new ConsumerBuilder();
		builder.consumer = new Consumer(brokerURL, destinationName,
				messageListener);
		return builder;
	}
	/**
	 * concurrentConsumers on a topic should be exactly 1
	 * concurrentConsumers on a queue should be > 1 for higher througput and loadbalancing
	 * concurrentConsumers on a queue should be = 1 for total ordering of message on the queue.
	 * @param concurrentConsumers
	 * @return
	 */
	public ConsumerBuilder withConcurrentConsumers(int concurrentConsumers) {
		consumer.setConcurrentConsumers(concurrentConsumers);
		return this;
	}
	/**
	 * Do not set this to true unless you have a reason to.
	 * @param watchTopicAdvisories
	 * @return
	 */
	public ConsumerBuilder withWatchTopicAdvisories(boolean watchTopicAdvisories) {
		consumer.setWatchTopicAdvisories(watchTopicAdvisories);
		return this;
	}
	
	/**
	 * Mandatory property for making a destination as a topic
	 * By Default this property is false. set to true if you want to consume from a topic.
	 * @param pubSubDomain
	 * @return
	 */
	public ConsumerBuilder withPubSubDomain(boolean pubSubDomain) {
		consumer.setPubSubDomain(pubSubDomain);
		return this;
	}
	/**
	 * Default acknowledge mode is Session.AUTO_ACKNOWLEDGE.
	 * needs a careful study of Acknowledge modes before you change this.
	 * @param sessionAcknowledgeMode
	 * @return
	 */
	public ConsumerBuilder withSessionAcknowledgeMode(int sessionAcknowledgeMode) {
		consumer.setSessionAcknowledgeMode(sessionAcknowledgeMode);
		return this;
	}

	/**
	 * Set durable subcription as true
	 * @param isDurable
	 * @return
	 */
	public ConsumerBuilder asDurableSubscriber(boolean isDurable) {
		consumer.setDurablesubscription(isDurable);
		return this;
	}	
	
	/**
	 * property to be set only for durable subscription on a topic
	 * @param clientId
	 * @return
	 */
	public ConsumerBuilder withClientId(String clientId) {
		consumer.setClientId(clientId);
		return this;
	}
	
	/**
	 * property to be set only for durable subscription on a topic
	 * @param clientId
	 * @return
	 */
	public ConsumerBuilder withDurableSubscriptionName(String durableSubscriptionName) {
		consumer.setDurableSubscriptionName(durableSubscriptionName);
		return this;
	}
	
	public ConsumerBuilder withCachedSessionSize(int cachedSessionSize) {
		consumer.setCachedSessionSize(cachedSessionSize);
		return this;
	}
	
	public ConsumerBuilder withMessageSelector(String messageSelector) {
		consumer.setMessageSelector(messageSelector);
		return this;
	}



	
	/**
	 * registers and starts the consumer
	 * @return
	 */
	public Consumer register(){
		consumer.init();
		return consumer;
	}
}
